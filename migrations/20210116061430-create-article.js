'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Articles', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      abstract: {
        type: Sequelize.STRING
      },
      web_url: {
        type: Sequelize.STRING
      },
      snippet: {
        type: Sequelize.STRING
      },
      lead_paragraph: {
        type: Sequelize.TEXT
      },
      source: {
        type: Sequelize.STRING(200)
      },
      pub_date: {
        type: Sequelize.DATE
      },
      document_type: {
        type: Sequelize.STRING(100)
      },
      news_desk: {
        type: Sequelize.STRING(200)
      },
      section_name: {
        type: Sequelize.STRING(50)
      },
      subsection_name: {
        type: Sequelize.STRING(100)
      },
      type_of_material: {
        type: Sequelize.STRING(100)
      },
      _id: {
        type: Sequelize.STRING(200)
      },
      word_count: {
        type: Sequelize.INTEGER
      },
      uri: {
        type: Sequelize.STRING(200)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Articles');
  }
};