'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Headlines', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      main: {
        type: Sequelize.STRING
      },
      kicker: {
        type: Sequelize.STRING
      },
      content_kicker: {
        type: Sequelize.STRING
      },
      print_headline: {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      seo: {
        type: Sequelize.STRING
      },
      sub: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Headlines');
  }
};