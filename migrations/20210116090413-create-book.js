'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Books', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      rank: {
        type: Sequelize.INTEGER
      },
      rank_last_week: {
        type: Sequelize.INTEGER
      },
      weeks_on_list: {
        type: Sequelize.INTEGER
      },
      asterisk: {
        type: Sequelize.INTEGER
      },
      dagger: {
        type: Sequelize.INTEGER
      },
      primary_isbn10: {
        type: Sequelize.STRING
      },
      primary_isbn13: {
        type: Sequelize.STRING
      },
      publisher: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.TEXT
      },
      price: {
        type: Sequelize.DOUBLE
      },
      title: {
        type: Sequelize.STRING
      },
      author: {
        type: Sequelize.STRING
      },
      contributor: {
        type: Sequelize.STRING
      },
      contributor_note: {
        type: Sequelize.STRING
      },
      book_image: {
        type: Sequelize.STRING
      },
      book_image_width: {
        type: Sequelize.INTEGER
      },
      book_image_height: {
        type: Sequelize.INTEGER
      },
      amazon_product_url: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Books');
  }
};