require('dotenv').config();
const Sequelize = require('sequelize');

module.exports = {
  development: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT,
    connectionTimeout: 0,
    pool: {
      max: 5,
      min: 1,
      idle: 200000,
      acquire: 200000
    },
    logging: true
  },
  production: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT,
    connectionTimeout: 0,
    pool: {
      max: 5,
      min: 1,
      idle: 200000,
      acquire: 200000
    }
  }
};
