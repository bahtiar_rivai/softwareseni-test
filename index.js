const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const path = require("path");
/* Read .env */
require("dotenv").config();

const app = express();
// for dynamic route
const routes = require("./helper/readfile.js").walkSync("./routes");
// Enable the CORS
app.use(cors());

// parse requests of content-type - application/json
app.use(bodyParser.json({ limit: "50mb" }));

// parse requests of content-type - application/x-www-form-urlencoded
app.use(
  bodyParser.urlencoded({
    limit: "50mb",
    extended: true,
    parameterLimit: 50000,
  })
);

// const array = [];
routes.forEach((file) => {
  let routePath = file.split(path.sep).slice(1, -1).join(path.sep);
  routePath = `/${routePath}/${path.parse(file).name}`;
  const routeFile = require(`./${file}`);
  app.use(routePath, routeFile);
});

app.use("/stat", (req, res) =>
  res.json({
    welcome: "start and ready",
  })
);

// Protect another url redirect to 404
app.use((req, res) => {
  res.status(404);
  if (req.accepts("json")) {
    res.send({ error: "404 Not found [I-52]" });
  }
});

// set port, listen for requests
const PORT = process.env.PORT || 8381;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
