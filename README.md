# README #

This project was generated with [NodeJS](https://nodejs.org/en/)

### Package ###
- express
- axios
- @hapi/joi
- body-parser
- cors
- dotenv
- fs
- lodash
- pg
- pg-hstore
- sequelize
- sequelize-cli

### Development server ###

* copy .env.example to .env (linux/macos : cp .env.example .env)

    setting .env

        NODE_ENV=

        PORT=

        DB_URL=

        DB_HOST=

        DB_PORT=

        DB_DATABASE=

        DB_USERNAME=

        DB_PASSWORD=

        DB_DIALECT=

* npm install
* install -g sequelize-cli
* sequelize db:migrate
* node index.php

### POSTMAN API TEST Software Seni ###

[API POSTMAN](https://www.getpostman.com/collections/6e5dabed85e1b4489c86)

{{url} = http://localhost:12081/api/

{{api-key}} = ga5GbXeNiItDSjN1evb2yeQn5ZU2lg3R

### API ARTICLE ###

* GET {{url}}article/get-nytimes?sort=newest&page=1&q=Trump&apikey={{api-key}}
* GET {{url}}article?page=1&by=id&sort=DESC&abstract
* GET {{url}}article/{id}
* POST {{url}}article/

### API BOOK ###

* GET {{url}}book/get-nytimes?apikey={{api-key}}
* GET {{url}}book?page=1&by=id&sort=DESC&title&author
* GET {{url}}book/{id}
* POST {{url}}book/