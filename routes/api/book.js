const express = require("express");
const axios = require("axios");
const {Book} = require('../../models');
const Paginator = require('../../helper/paginator');
const Joi = require("@hapi/joi");
const router = express.Router();

router.get("/get-nytimes", async (req, res) => {
    try {
      axios.get(`https://api.nytimes.com/svc/books/v3/lists/current/hardcover-fiction.json?api-key=${apikey}`).then(r => {
        const data = r.data.results.books;
        let book = {};
        r.data.results.books.map(async(d) => {
          console.log(d.rank);
          let x = await Book.findOne({ where: { rank: d.rank } }).then(async (a) => {
            if (a == null) {
                book = {
                          rank: d.rank,
                          rank_last_week: d.rank_last_week,
                          weeks_on_list: d.weeks_on_list,
                          asterisk: d.asterisk,
                          dagger: d.dagger,
                          primary_isbn10: d.primary_isbn10,
                          primary_isbn13: d.primary_isbn13,
                          publisher: d.publisher,
                          description: d.description,
                          price: d.price,
                          title: d.title,
                          author: d.author,
                          contributor: d.contributor,
                          contributor_note: d.contributor_note,
                          book_image: d.book_image,
                          book_image_width: d.book_image_width,
                          book_image_height: d.book_image_height,
                          amazon_product_url: d.amazon_product_url,
                          uri: d.uri
                        };
              await Book.create(book);
            }
          });
        })
        res.json({
          success: true,
          message: 'success get data',
          data
        })
      });
  
    } catch (error) {
      res.status(422).json({ success: false, message: 'Ops something wrong.' });
    }
  });

  router.get('/', async (req, res) => {
      let { page, by, sort = 'ASC', limit } = req.query;
      page = Number(page || 1);
      limit = Number(limit || 10);
      const paginator = new Paginator(page, limit);
      const offset = paginator.getOffset();
  
      if (!by) by = 'id';
      const array = ['id', 'title', 'description', 'author', 'contributor'];
      if (array.indexOf(by) < 0) {
        by = 'id';
      }
  
      if (sort.toLowerCase() !== 'desc') sort = 'ASC';
      else sort = 'DESC';
  
      const { title, author } = req.query;
      const where = {};
      if (title) {
        Object.assign(where, {
          title: {
            [Op.iLike]: `%${title}%`
          }
        });
      }
      if (author) {
        Object.assign(where, {
          author: {
            [Op.iLike]: `%${author}%`
          }
        });
      }

      return Book.findAndCountAll({
        attributes: {
          exclude: ['createdAt', 'updatedAt']
        },
        where,
        offset,
        limit,
        order: [[by, sort]]
      })
        .then(data => {
          const { count } = data;
          const items = data.rows;
          paginator.setCount(count);
          paginator.setData(items);
          return res.json({
            status: true,
            pagination: paginator.getPaginator()
          });
        })
        .catch(() => res.status(422).send({ success: false, message: 'Book not found.' }));
    }
  );

function validateBook(book) 
{ 
    const JoiSchema = Joi.object({ 
      rank: Joi.number().integer().required(), 
      rank_last_week: Joi.number().integer().optional(), 
      weeks_on_list: Joi.number().integer().optional(), 
      asterisk: Joi.number().integer().optional(), 
      dagger: Joi.number().integer().optional(), 
      primary_isbn10: Joi.string().optional(),
      primary_isbn13: Joi.string().optional(),
      publisher: Joi.string().optional(),
      description: Joi.string().optional(),
      price: Joi.number().optional(),
      title: Joi.string().min(25).max(250).required(), 
      author: Joi.string().min(5).max(60).optional(),
      contributor: Joi.string().optional(),
      contributor_note: Joi.string().optional(),
      book_image: Joi.string().optional(),
      book_image_width: Joi.number().optional(),
      book_image_height: Joi.number().optional(),
      amazon_product_url: Joi.string().optional(),
    }).options({ abortEarly: false }); 
  
    return JoiSchema.validate(book) 
} 

router.post('/', async (req, res) => {
    const book = { 
      rank: req.body.rank, 
      rank_last_week: req.body.rank_last_week, 
      weeks_on_list: req.body.weeks_on_list, 
      asterisk: req.body.asterisk,
      dagger: req.body.dagger,
      primary_isbn10: req.body.primary_isbn10,
      primary_isbn13: req.body.primary_isbn13,
      publisher: req.body.publisher,
      description: req.body.description,
      price: req.body.price,
      title: req.body.title,
      author: req.body.author,
      contributor: req.body.contributor,
      contributor_note: req.body.contributor_note,
      book_image: req.body.book_image,
      book_image_width: req.body.book_image_width,
      book_image_height: req.body.book_image_height,
      amazon_product_url: req.body.amazon_product_url
    }

    response = validateBook(book); 
  
    if(response.error) {   
      return res.status(422).json({ success: false, message: response.error });
    } else { 
      return Book.create(book)
        .then(data => res.json({ success: true, message: 'success', data }))
        .catch(() => res.status(422).json({ success: false, message: 'Error, something wrong.' }));
    } 
  }
);

router.get('/:id', async (req, res) => {
    const { id } = req.params;
    return Book.findOne({
      where: { id },
      order: [['createdAt', 'desc']]
    })
      .then(data => res.json({ success: true, message: 'success', data }))
      .catch(() => res.status(422).json({ success: false, message: 'Book not found.' }));
  }
);

module.exports = router;
