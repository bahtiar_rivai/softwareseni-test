const express = require("express");
const axios = require("axios");
const { Article } = require('../../models');
const Paginator = require('../../helper/paginator');
const Joi = require("@hapi/joi");
const router = express.Router();

router.get("/get-nytimes", async (req, res) => {
  try {
    let { page, sort = 'newest', q, apikey } = req.query;
    page = Number(page || 1);
    axios.get(`https://api.nytimes.com/svc/search/v2/articlesearch.json?page=${page}&sort=${sort}&q=${q}&api-key=${apikey}`).then(r => {
      const data = r.data.response.docs;
      let article = {};
      r.data.response.docs.map(async(d) => {
        let x = await Article.findOne({ where: { uri: d.uri } }).then(async (a) => {
          if (a == null) {
            article = {
                        abstract: d.abstract,
                        web_url: d.web_url,
                        snippet: d.snippet,
                        lead_paragraph: d.lead_paragraph,
                        lead_paragraph: d.lead_paragraph,
                        source: d.source,
                        pub_date: d.pub_date,
                        document_type: d.document_type,
                        news_desk: d.news_desk,
                        subsection_name: d.subsection_name,
                        type_of_material: d.type_of_material,
                        _id: d._id,
                        word_count: d.word_count,
                        uri: d.uri
                      };
            await Article.create(article);
          }
        });
      })
      res.json({
        success: true,
        message: 'success get data',
        data
      })
    });

  } catch (error) {
    res.status(422).json({ success: false, message: 'Ops something wrong.' });
  }
});

router.get('/', async (req, res) => {
    let { page, by, sort = 'ASC', limit } = req.query;
    page = Number(page || 1);
    limit = Number(limit || 10);
    const paginator = new Paginator(page, limit);
    const offset = paginator.getOffset();

    if (!by) by = 'id';
    const array = ['id', 'abstract'];
    if (array.indexOf(by) < 0) {
      by = 'id';
    }

    if (sort.toLowerCase() !== 'desc') sort = 'ASC';
    else sort = 'DESC';

    const { abstract } = req.query;
    const where = {};
    if (abstract) {
      Object.assign(where, {
        abstract: {
          [Op.iLike]: `%${abstract}%`
        }
      });
    }

    return Article.findAndCountAll({
      attributes: {
        exclude: ['createdAt', 'updatedAt']
      },
      where,
      offset,
      limit,
      order: [[by, sort]]
    })
      .then(data => {
        const { count } = data;
        const items = data.rows;
        paginator.setCount(count);
        paginator.setData(items);
        return res.json({
          status: true,
          pagination: paginator.getPaginator()
        });
      })
      .catch(() => res.status(422).send({ success: false, message: 'Article not found.' }));
    }
  );

function validateArticle(article) 
{ 
    const JoiSchema = Joi.object({ 
      abstract: Joi.string().required(), 
      web_url: Joi.string().optional(), 
      snippet: Joi.string().optional(), 
      lead_paragraph: Joi.string().optional(), 
      source: Joi.string().optional(),
      document_type: Joi.string().optional(),
      news_desk: Joi.string().optional(),
      section_name: Joi.string().optional(),
      subsection_name: Joi.string().optional(),
      type_of_material: Joi.string().optional(),
      _id: Joi.string().optional(),
      word_count: Joi.number().optional(),
      uri: Joi.string().optional(),
    }).options({ abortEarly: false }); 
  
    return JoiSchema.validate(article) 
} 

router.post('/', async (req, res) => {
    const article = { 
      abstract: req.body.abstract, 
      web_url: req.body.web_url, 
      snippet: req.body.snippet, 
      lead_paragraph: req.body.lead_paragraph,
      source: req.body.source,
      document_type: req.body.document_type,
      news_desk: req.body.news_desk,
      section_name: req.body.section_name,
      subsection_name: req.body.subsection_name,
      type_of_material: req.body.type_of_material,
      _id: req.body._id,
      word_count: req.body.word_count,
      uri: req.body.uri
    }

    response = validateArticle(article); 
  
    if(response.error) {   
      return res.status(422).json({ success: false, message: response.error });
    } else { 
      return Article.create(article)
        .then(data => res.json({ success: true, message: 'success', data }))
        .catch(() => res.status(422).json({ success: false, message: 'Error, something wrong.' }));
    } 
  }
);

router.get('/:id', async (req, res) => {
    const { id } = req.params;
    return Article.findOne({
      where: { id },
      order: [['createdAt', 'desc']]
    })
      .then(data => res.json({ success: true, message: 'success', data }))
      .catch(() => res.status(422).json({ success: false, message: 'Article not found.' }));
  }
);

module.exports = router;
