'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Headline extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Headline.init({
    main: DataTypes.STRING,
    kicker: DataTypes.STRING,
    content_kicker: DataTypes.STRING,
    print_headline: DataTypes.STRING,
    name: DataTypes.STRING,
    seo: DataTypes.STRING,
    sub: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Headline',
  });
  return Headline;
};