'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Book extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Book.init({
    rank: DataTypes.INTEGER,
    rank_last_week: DataTypes.INTEGER,
    weeks_on_list: DataTypes.INTEGER,
    asterisk: DataTypes.INTEGER,
    dagger: DataTypes.INTEGER,
    primary_isbn10: DataTypes.STRING,
    primary_isbn13: DataTypes.STRING,
    publisher: DataTypes.STRING,
    description: DataTypes.TEXT,
    price: DataTypes.DOUBLE,
    title: DataTypes.STRING,
    author: DataTypes.STRING,
    contributor: DataTypes.STRING,
    contributor_note: DataTypes.STRING,
    book_image: DataTypes.STRING,
    book_image_width: DataTypes.INTEGER,
    book_image_height: DataTypes.INTEGER,
    amazon_product_url: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Book',
  });
  return Book;
};