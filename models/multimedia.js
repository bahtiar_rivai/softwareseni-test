'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Multimedia extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Multimedia.init({
    articleId: DataTypes.INTEGER,
    rank: DataTypes.INTEGER,
    subtype: DataTypes.STRING,
    caption: DataTypes.STRING,
    credit: DataTypes.STRING,
    type: DataTypes.STRING,
    url: DataTypes.STRING,
    height: DataTypes.INTEGER,
    width: DataTypes.INTEGER,
    legacy: DataTypes.TEXT,
    subType: DataTypes.STRING,
    subType: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Multimedia',
  });
  return Multimedia;
};