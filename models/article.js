'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Article extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Article.init({
    abstract: DataTypes.STRING,
    web_url: DataTypes.STRING,
    snippet: DataTypes.STRING,
    lead_paragraph: DataTypes.TEXT,
    source: DataTypes.STRING,
    pub_date: DataTypes.DATE,
    document_type: DataTypes.STRING,
    news_desk: DataTypes.STRING,
    section_name: DataTypes.STRING,
    subsection_name: DataTypes.STRING,
    type_of_material: DataTypes.STRING,
    _id: DataTypes.STRING,
    word_count: DataTypes.INTEGER,
    uri: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Article',
  });
  return Article;
};